﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("ParkingChargeCalculatorUnitTests")]
namespace ParkingChargeCalculator
{
    public enum StayTypeEnum
    {
        ShortStay,
        LongStay
    }
}