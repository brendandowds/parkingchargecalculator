﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingChargeCalculator
{
    public class LongStayCalculator : BaseStayCalculator, IStayCalculator
    {
        DateTime start;
        DateTime end;
        double dayRate = 7.5;

        public LongStayCalculator(DateTime start, DateTime end)
        {
            if (start > end)
                throw new ArgumentException($"Start time ({start}) should be before the end time ({end}) of the stay");

            this.start = start;
            this.end = end;
        }

        public string CalculateCharge()
        {
            if (start == end)
                return FormattedCharge(0);

            double numberOfDays = (this.end.Date - this.start.Date).TotalDays + 1;
            return FormattedCharge(numberOfDays * this.dayRate);
        }
    }
}
