﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingChargeCalculator
{
    internal class StayCalculatorFactory
    {
        internal IStayCalculator GetStayCalculator(StayTypeEnum stayType, DateTime start, DateTime end)
        {
            switch (stayType)
            {
                case StayTypeEnum.ShortStay:
                    return new ShortStayCalculator(start, end);
                case StayTypeEnum.LongStay:
                    return new LongStayCalculator(start, end);
                default:
                    throw new ArgumentException($"Invalid stay type ({stayType}) selected.");
            }
        }
    }
}
