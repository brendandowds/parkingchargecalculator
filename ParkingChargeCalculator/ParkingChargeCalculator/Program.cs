﻿using System;
using System.Collections.Generic;

namespace ParkingChargeCalculator
{
    class Program
    {

        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the Parking Charge Calculator");
            Console.WriteLine("Rates currently are:");
            Console.WriteLine("\tShort stay: £1.10/hr between 0800 and 1800 weekdays");
            Console.WriteLine("\tLong stay:  £7.50/day or part day\n");
            Console.WriteLine("Unless otherwise stated charges will be rounded up to the nearest hour (for short stay) or day (for long stay)");

            foreach (string example in GetExamples())
                Console.WriteLine($"{example}");

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private static IEnumerable<string> GetExamples()
        {        
            IList<string> examples = new List<string>();
            examples.Add("Example charges:");


            StayCalculatorFactory factory = new StayCalculatorFactory();
            examples.Add(GetExample(factory, StayTypeEnum.ShortStay, new DateTime(2020, 11, 7, 7, 0, 0), new DateTime(2020, 11, 8, 23, 30, 0)));
            examples.Add(GetExample(factory, StayTypeEnum.ShortStay, new DateTime(2017, 9, 7, 16, 50, 0), new DateTime(2017, 9, 9, 19, 15, 0)));
            examples.Add(GetExample(factory, StayTypeEnum.LongStay, new DateTime(2017, 9, 7, 7, 50, 0), new DateTime(2017, 9, 9, 5, 20, 0)));

            return examples;
        }

        private static string GetExample(StayCalculatorFactory factory, StayTypeEnum stayType, DateTime start, DateTime end)
        {
            IStayCalculator stayCalculator = factory.GetStayCalculator(stayType, start, end);

            string charge = stayCalculator.CalculateCharge();
            string text = string.Format("{0} from {1} on a {2} ({3}) until {4} on {5} ({6}) - {7}",
                                        StayTypeEnum.LongStay,
                                        start.TimeOfDay,
                                        start.DayOfWeek,
                                        start.Date.ToShortDateString(),
                                        end.TimeOfDay,
                                        end.DayOfWeek,
                                        end.Date,
                                        charge);

            return text;
        }
    }
}
