﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingChargeCalculator
{
    public class BaseStayCalculator
    {
        protected string FormattedCharge(double exactCharge)
        {
            return exactCharge.ToString("C");
        }
    }
}
