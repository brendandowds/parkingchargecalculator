﻿namespace ParkingChargeCalculator
{
    public interface IStayCalculator
    {
        string CalculateCharge();
    }
}