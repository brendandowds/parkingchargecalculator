﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingChargeCalculator
{
    public class ShortStayCalculator : BaseStayCalculator, IStayCalculator
    {
        DateTime start;
        DateTime end;
        double dayRate = 1.1;

        IList<DayOfWeek> chargableDays = new List<DayOfWeek>()
        {
            DayOfWeek.Monday,
            DayOfWeek.Tuesday,
            DayOfWeek.Wednesday,
            DayOfWeek.Thursday,
            DayOfWeek.Friday,
        };

        TimeSpan chargeStartTime = new TimeSpan(8, 0, 0);
        TimeSpan chargeEndTime = new TimeSpan(18, 0, 0);
        double fullDay;

        public ShortStayCalculator(DateTime start, DateTime end)
        {
            if (start > end)
                throw new ArgumentException($"Start time ({start}) should be before the end time ({end}) of the stay");

            this.start = start;
            this.end = end;
            fullDay = (chargeEndTime - chargeStartTime).TotalHours;
        }

        public string CalculateCharge()
        {
            if (start == end)
                return FormattedCharge(0);

            double numberOfHours = 0;

            // Get portion of first day, if chargable
            if (chargableDays.Contains(start.DayOfWeek) && start.TimeOfDay < chargeEndTime)
            {
                // Start from day start time or parking start time which is later
                TimeSpan dayStartTime = (start.TimeOfDay > chargeStartTime) ? start.TimeOfDay : chargeStartTime;

                // End at day end time or parking end time if it's the same day, whichever is earlier
                TimeSpan dayEndTime = (end.Date == start.Date && end.TimeOfDay < chargeEndTime) ? end.TimeOfDay : chargeEndTime;

                numberOfHours += (dayEndTime - dayStartTime).TotalHours;
            }

            // Get all the chargable days between start and end, if any
            DateTime date = start.AddDays(1);
            while (date < end.Date)
            {
                if (chargableDays.Contains(date.DayOfWeek))
                    numberOfHours += fullDay;

                date = date.AddDays(1);
            }

            // Get portion of end day, if chargable
            // And only if not the same date as the start date, otherwise we'll double charge
            if (end.Date > start.Date &&
                chargableDays.Contains(end.DayOfWeek) &&
                end.TimeOfDay > chargeStartTime)
            {
                // End at day end time or parking end time, whichever is earlier
                TimeSpan dayEndTime = (end.TimeOfDay < chargeEndTime) ? end.TimeOfDay : chargeEndTime;

                numberOfHours += (dayEndTime - chargeStartTime).TotalHours;
            }

            return FormattedCharge(numberOfHours * dayRate);
        }
    }
}
