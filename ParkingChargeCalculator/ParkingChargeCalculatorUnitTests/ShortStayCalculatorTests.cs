﻿
using ParkingChargeCalculator;
using System;
using System.Collections.Generic;
using Xunit;

namespace ParkingChargeCalculatorUnitTests
{
    public class ShortStayCalculatorTests
    {
        [Fact]
        public void Test_Constructor_EndBeforeStart_Exception()
        {
            DateTime start = DateTime.Now;
            Assert.Throws<ArgumentException>(() => new ShortStayCalculator(start, start.AddSeconds(-1)));
        }

        [Theory]
        [MemberData(nameof(SuccessTests))]
        public void Test_CalculateCharge_Success(DateTime start, DateTime end, string expected)
        {
            ShortStayCalculator calculator = new ShortStayCalculator(start, end);

            string actual = calculator.CalculateCharge();

            Assert.Equal(expected, actual);
        }

        public static IEnumerable<object[]> SuccessTests
        {
            get
            {
                return new List<object[]>
                {
                    new object[] { new DateTime(2020, 11, 9, 12, 30, 0), new DateTime(2020, 11, 9, 12, 30, 0), "£0.00" }, // Same start and end time
                    new object[] { new DateTime(2020, 11, 9, 5, 30, 0), new DateTime(2020, 11, 9, 20, 0, 0), "£11.00" }, // Start before start charge time, end after last charge time, same chargable day
                    new object[] { new DateTime(2020, 11, 9, 12, 30, 0), new DateTime(2020, 11, 9, 20, 0, 0), "£6.05" }, // Start before last charge time, end after last charge time, same chargable day
                    new object[] { new DateTime(2020, 11, 9, 19, 30, 0), new DateTime(2020, 11, 9, 20, 0, 0), "£0.00" }, // Start after last charge time, end after last charge time, same chargable day
                    
                    new object[] { new DateTime(2020, 11, 9, 5, 30, 0), new DateTime(2020, 11, 9, 14, 15, 0), "£6.88" }, // Start before start charge time, end before last charge time, same chargable day
                    new object[] { new DateTime(2020, 11, 9, 12, 30, 0), new DateTime(2020, 11, 9, 14, 15, 0), "£1.93" }, // Start before last charge time, end before last charge time, same chargable day

                    new object[] { new DateTime(2020, 11, 7), new DateTime(2020, 11, 7, 23, 59, 59), "£0.00" }, // Saturday not charged
                    new object[] { new DateTime(2020, 11, 8), new DateTime(2020, 11, 8, 23, 59, 59), "£0.00" }, // Sunday not charged
                    new object[] { new DateTime(2020, 11, 6, 12, 0, 0), new DateTime(2020, 11, 10, 12, 0, 0), "£22.00" }, // Over weekend, weekend not charged
                    
                    new object[] { new DateTime(2020, 11, 9, 5, 30, 0), new DateTime(2020, 11, 12, 20, 0, 0), "£44.00" }, // Start before start charge time, end after last charge time, different days
                    new object[] { new DateTime(2020, 11, 9, 12, 30, 0), new DateTime(2020, 11, 12, 20, 0, 0), "£39.05" }, // Start before last charge time, end after last charge time, different days
                    new object[] { new DateTime(2020, 11, 9, 19, 30, 0), new DateTime(2020, 11, 12, 20, 0, 0), "£33.00" }, // Start after last charge time, end after last charge time, different days
                    
                    new object[] { new DateTime(2020, 11, 9, 5, 30, 0), new DateTime(2020, 11, 12, 14, 15, 0), "£39.88" }, // Start before start charge time, end after start charge time, different days
                    new object[] { new DateTime(2020, 11, 9, 12, 30, 0), new DateTime(2020, 11, 12, 14, 15, 0), "£34.93" }, // Start before last charge time, end after start charge time, different days
                    new object[] { new DateTime(2020, 11, 9, 19, 30, 0), new DateTime(2020, 11, 12, 14, 15, 0), "£28.88" }, // Start after last charge time, end after start charge time, different days
                    
                    new object[] { new DateTime(2020, 11, 9, 5, 30, 0), new DateTime(2020, 11, 12, 5, 30, 0), "£33.00" }, // Start before start charge time, end before start charge time, different days
                    new object[] { new DateTime(2020, 11, 9, 12, 30, 0), new DateTime(2020, 11, 12, 5, 30, 0), "£28.05" }, // Start before last charge time, end before start charge time, different days
                    new object[] { new DateTime(2020, 11, 9, 19, 30, 0), new DateTime(2020, 11, 12, 5, 30, 0), "£22.00" }, // Start after last charge time, end before charge time, different days

                    new object[] { new DateTime(2017, 9, 7, 16, 50, 0), new DateTime(2017, 9, 9, 19, 15, 0), "£12.28" }, // Given example
                };
            }
        }
    }
}

