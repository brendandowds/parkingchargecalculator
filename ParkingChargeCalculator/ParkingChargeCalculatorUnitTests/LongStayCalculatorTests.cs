﻿using ParkingChargeCalculator;
using System;
using System.Collections.Generic;
using Xunit;

namespace ParkingChargeCalculatorUnitTests
{
    public class LongStayCalculatorTests
    {
        [Fact]
        public void Test_Constructor_EndBeforeStart_Exception()
        {
            DateTime start = DateTime.Now;
            Assert.Throws<ArgumentException>(() => new LongStayCalculator(start, start.AddSeconds(-1)));
        }

        [Theory]
        [MemberData(nameof(SuccessTests))]
        public void Test_CalculateCharge_Success(DateTime start, DateTime end, string expected)
        {
            LongStayCalculator calculator = new LongStayCalculator(start, end);

            string actual = calculator.CalculateCharge();

            Assert.Equal(expected, actual);
        }

        public static IEnumerable<object[]> SuccessTests
        {
            get
            {
                return new List<object[]>
                {
                    new object[] { new DateTime(2020, 11, 7), new DateTime(2020, 11, 7), "£0.00" }, // Same start and end
                    new object[] { new DateTime(2020, 11, 7), new DateTime(2020, 11, 7, 23, 59, 59), "£7.50" }, // Just short of one 24hr period, on a Saturday
                    new object[] { new DateTime(2020, 11, 8), new DateTime(2020, 11, 8, 23, 59, 59), "£7.50" }, // Just short of one 24hr period, on a Sunday
                    new object[] { new DateTime(2020, 11, 9), new DateTime(2020, 11, 9, 23, 59, 59), "£7.50" }, // Just short of one 24hr period, on a week day
                    new object[] { new DateTime(2020, 11, 9), new DateTime(2020, 11, 10, 0, 0, 1), "£15.00" }, // Just over one 24hr period
                    new object[] { new DateTime(2020, 11, 1, 13, 0, 0), new DateTime(2020, 11, 1, 13, 0, 1), "£7.50" }, // One day for just a minute
                    new object[] { new DateTime(2020, 11, 1, 13, 0, 0), new DateTime(2020, 11, 2, 14, 0, 1), "£15.00" }, // 24hr and 1 min
                    new object[] { new DateTime(2020, 11, 1), new DateTime(2020, 11, 7, 23, 59, 59), "£52.50" }, // A week

                    new object[] { new DateTime(2017, 9, 7, 7, 50, 0), new DateTime(2017, 9, 9, 5, 20, 0), "£22.50" }
            };
            }
        }
    }
}
