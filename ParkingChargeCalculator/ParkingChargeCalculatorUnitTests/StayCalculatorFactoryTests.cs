using Xunit;
using ParkingChargeCalculator;
using System;

namespace ParkingChargeCalculatorUnitTests
{
    public class StayCalculatorFactoryTests
    {
        [Theory]
        [InlineData(StayTypeEnum.LongStay, typeof(LongStayCalculator))]
        [InlineData(StayTypeEnum.ShortStay, typeof(ShortStayCalculator))]
        public void Test_GetStayCalculator(StayTypeEnum stayType, Type expectedType)
        {
            StayCalculatorFactory factory = new StayCalculatorFactory();
            DateTime start = DateTime.Now;
            IStayCalculator actual = factory.GetStayCalculator(stayType, start, start.AddHours(1));

            Assert.IsType(expectedType, actual);
        }
    }
}
